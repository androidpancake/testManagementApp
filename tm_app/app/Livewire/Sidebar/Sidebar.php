<?php

namespace App\Livewire\Sidebar;

use App\Models\Project;
use App\Models\TestLevel;
use Livewire\Component;

class Sidebar extends Component
{
    public $sidebar;

    public function sit()
    {
        $testlevel = TestLevel::where('type', 'SIT')->firstOrFail();
        $data = Project::create([
            'user_id' => auth()->id(),
            'test_level_id' => $testlevel->id,
            'published' => 'draft'
        ]);

        $this->sidebar = $data->test_level->type;

        return redirect()->route('form', $data->id);
    }

    public function uat()
    {
        $testlevel = TestLevel::where('type', 'UAT')->firstOrFail();

        $data = Project::create([
            'user_id' => auth()->id(),
            'test_level_id' => $testlevel->id,
            'published' => 'draft'
        ]);

        $this->sidebar = $data->test_level->type;

        return redirect()->route('form', $data->id);
    }

    public function pir()
    {
        $testlevel = TestLevel::where('type', 'PIR')->firstOrFail();

        $data = Project::create([
            'user_id' => auth()->id(),
            'test_level_id' => $testlevel->id,
            'published' => 'draft'
        ]);

        $this->sidebar = $data->test_level->type;

        return redirect()->route('form', $data->id);
    }

    public function render()
    {
        return view('livewire.sidebar.sidebar');
    }
}
