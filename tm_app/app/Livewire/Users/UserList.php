<?php

namespace App\Livewire\Users;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Livewire\Attributes\Lazy;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\Attributes\On;
use Livewire\Attributes\Locked;

class UserList extends Component
{
    use WithPagination;

    #[Locked]
    public $user_id;
    public $name;
    public $username;
    public $unit;

    public $editMode = false;
    public $user;

    public $search;

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->user_id = $id;
        $this->name = $user->name;
        $this->username = $user->username;
        $this->unit = $user->unit;
        $this->editMode = true;
    }

    public function update()
    {
        $validated = $this->validate([
            'name' => 'required',
            'username' => 'required',
            'unit' => 'required'
        ]);

        $user = User::find($this->user_id);
        $user->update([
            'username' => $this->username,
            'unit' => $this->unit
        ]);
        session()->flash('success', 'Sukses update data user');
        $this->editMode = false;
    }

    public function cancel()
    {
        $this->editMode = false;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->roles()->detach();
        $user->delete();

        $this->dispatch(session()->flash('success', 'Sukses hapus data user'));
    }

    public function render()
    {
        $users = User::with('roles')->whereHas('roles', function ($query) {
            $query->where('name', 'USER');
        });

        if (!empty($this->search)) {
            $users->where('name', 'like', '%' . $this->search . '%')
                ->orWhere('username', 'like', '%' . $this->search . '%');
        }

        $users = $users->paginate(10);

        return view('livewire.users.index', [
            'users' => $users
        ]);
    }
}
