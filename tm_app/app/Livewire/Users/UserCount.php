<?php

namespace App\Livewire\Users;

use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\Attributes\On;

class UserCount extends Component
{
    public $scope;

    protected $listeners = ['scopeUpdated' => 'updatedScope'];

    public function updatedScope($value)
    {
        $this->scope = $value;
    }

    public function render()
    {
        $usercount = $this->getCountUser($this->scope);

        return view('livewire.users.user-count', [
            'usercount' => $usercount
        ]);
    }

    public function getCountUser($scope)
    {
        if ($scope === 'Today' || $scope === 'today') {
            return User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', 'USER');
            })->where('created_at', Carbon::today())->count();
        } elseif ($scope === 'all' || $scope === 'All') {
            return User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', 'USER');
            })->count();
        }
    }
}
