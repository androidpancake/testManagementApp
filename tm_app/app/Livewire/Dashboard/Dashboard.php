<?php

namespace App\Livewire\Dashboard;

use App\Livewire\Users\UserCount;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Livewire\Component;

class Dashboard extends Component
{
    public $sitData;
    public $uatData;
    public $pirData;
    public $countUser;
    public $chartData = [];
    public $labels;
    public $value;
    public $scope= 'Today';

    // public function updatedScope()
    // {
    //     // dd($this->scope);
    //     $this->dispatch('scopeUpdated', scope: $this->scope);
    // }

    public function mount()
    {
        $this->sitData = Project::whereHas('test_level', function ($query) {
            $query->where('type', '=', 'SIT');
        })->count();

        $this->uatData = Project::whereHas('test_level', function ($query) {
            $query->where('type', '=', 'SIT');
        })->count();

        $this->pirData = Project::whereHas('test_level', function ($query) {
            $query->where('type', '=', 'PIR');
        })->count();

        $this->chartData = [
            'series' => [$this->sitData, $this->uatData, $this->pirData],
            'labels' => ['SIT', 'UAT', 'PIR'],
            'scope' => $this->scope
        ];

        // dd($this->chartData);
    }

    public function render()
    {
        return view('livewire.dashboard.dashboard', [
            'chartData' => $this->chartData,
        ]);
    }
}
