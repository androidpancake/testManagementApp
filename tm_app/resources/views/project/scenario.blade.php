@extends('base.app')
@section('content')
@if($projectData)
<div class="h-screen overflow-auto">
    @if(session('success'))
    <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
        <span class="font-medium">Success!</span> {{ session('success') }}.
    </div>
    @endif

    @if(session('case'))
    <div class="p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert">
        <span class="font-medium">Success!</span> {{ session('success') }}.
    </div>
    @endif

    <div class="py-2">
        <livewire:head.head title="CRUD" description="Scenario-Case-Step" />
    </div>
    <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
        <ul class="flex flex-wrap -mb-px text-sm font-medium text-center" id="default-tab" data-tabs-toggle="#default-tab-content" role="tablist">
            <li class="me-2" role="presentation">
                <button class="inline-block p-4 border-b-2 rounded-t-lg" id="scenario-tab" data-tabs-target="#scenario" type="button" role="tab" aria-controls="scenario" aria-selected="false">Test</button>
            </li>
            <li class="me-2" role="presentation">
                <button class="inline-block p-4 border-b-2 rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" id="existing-tab" data-tabs-target="#existing" type="button" role="tab" aria-controls="existing" aria-selected="false">Existing</button>
            </li>
            <li class="me-2" role="presentation">
                <button class="inline-block p-4 border-b-2 rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" id="settings-tab" data-tabs-target="#settings" type="button" role="tab" aria-controls="settings" aria-selected="false">Case</button>
            </li>
            <li role="presentation">
                <button class="inline-block p-4 border-b-2 rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" id="contacts-tab" data-tabs-target="#contacts" type="button" role="tab" aria-controls="contacts" aria-selected="false">Step</button>
            </li>
        </ul>
    </div>
    <div id="default-tab-content">
        <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="scenario" role="tabpanel" aria-labelledby="scenario-tab">
            <div class="relative overflow-auto shadow-md p-2">
                <table id="tableTest" class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-6 py-3">
                                No.
                            </th>
                            <th scope="col" class="px-16 py-3">
                                Scenario
                            </th>
                            <th scope="col" class="px-24 py-3" colspan="2">
                                Test Case
                            </th>
                            <th scope="col" class="px-12 py-3">
                                Test Step ID
                            </th>
                            <th scope="col" class="px-24 py-3">
                                Test Step
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Expected Result
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Category (positive/negative)
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Severity (High/Medium/Low)
                            </th>
                            <th scope="col" class="px-6 py-3" colspan="2">
                                Status (Passed/Failed)
                            </th>
                            <th scope="col" class="px-6 py-3" colspan="2">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <livewire:Project.ScenarioComponent :id="$projectData->id" />
                    </tbody>
                </table>
            </div>
        </div>
        <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="existing" role="tabpanel" aria-labelledby="existing-tab">
            @include('project.scenario-form')
        </div>
        <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="settings" role="tabpanel" aria-labelledby="settings-tab">
            @include('project.case')
        </div>
        <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="contacts" role="tabpanel" aria-labelledby="contacts-tab">
            @include('project.step')
        </div>
    </div>

    @else
    <div class="flex flex-col">
        <div class="bg-white p-2 rounded-lg mb-2">
            <p>Tidak ada data</p>
        </div>
    </div>
    @endif
</div>
@endsection