<div class="pt-4 h-screen space-y-2">
    <!-- title -->
    <livewire:head.head title="Dashboard" description="Selamat Datang" user_name="{{ auth()->user()->name }}" />
    <!-- section1 -->
    <div class="flex flex-col gap-2 lg:flex-row">
        <!-- Chart section -->
        <div class="w-full lg:w-full bg-white rounded-lg dark:bg-gray-800 p-4 md:p-6">

            <div class="flex justify-between mb-3">
                <div class="flex justify-center items-center">
                    <h5 class="text-md font-medium leading-none text-gray-900 dark:text-white pe-1">Test Type Summary</h5>
                </div>
            </div>

            <!-- Chart -->
            <div class="py-6" id="donut-chart"></div>
        </div>
        @if(auth()->user()->roles->first()->name === 'USER')
        <!-- Project section -->
        <div class="w-full lg:w-3/5 bg-white rounded-lg dark:bg-gray-800 p-4">

            <!-- project -->
            <div class="relative overflow-x-auto">
                <livewire:project.project />
            </div>
        </div>
        @endif
    </div>

    @if(auth()->user()->roles->first()->name === 'ADMIN')
    <!-- section2 -->
    <div class="flex flex-col w-full gap-2 lg:flex-row">
        <!-- summary -->
        <div class="w-full lg:max-w-sm bg-white rounded-lg dark:bg-gray-800 p-4 md:p-6">

            <div class="flex justify-between mb-3">
                <div class="flex justify-center items-center">
                    <h5 class="text-md font-medium leading-none text-gray-600 dark:text-white pe-1">User Count</h5>
                </div>
                <select wire:click="$dispatchTo('UserCount', 'scopeUpdated', { value: {{ $scope }}})" class="bg-gray-200 text-sm text-gray-500 px-2 py-1 font-semibold rounded dark:bg-gray-900 dark:border-gray-700">
                    <option value="today" selected>Today</option>
                </select>
            </div>

            <!-- count users data -->
            <div>
                <div class="font-semibold text-5xl text-gray-900">
                    <livewire:users.usercount :scope="$scope" />

                </div>
            </div>
        </div>
        <!-- table -->
        <div class="w-full bg-white rounded-lg dark:bg-gray-800 p-4">
            <!-- table -->
            <livewire:users.user-list />
        </div>
    </div>
    @endif
</div>
<script>
    // ApexCharts options and config
    var chartData = <?php echo json_encode($chartData) ?>;
    window.addEventListener("load", function() {
        const getChartOptions = () => {
            return {
                series: chartData.series,
                colors: ["#1C64F2", "#16BDCA", "#FDBA8C", "#E74694"],
                chart: {
                    height: 320,
                    width: "100%",
                    type: "donut",
                },
                stroke: {
                    colors: ["transparent"],
                    lineCap: "",
                },
                plotOptions: {
                    pie: {
                        donut: {
                            labels: {
                                show: true,
                                name: {
                                    show: true,
                                    fontFamily: "Inter, sans-serif",
                                    offsetY: 20,
                                },
                                total: {
                                    showAlways: true,
                                    show: true,
                                    label: "Total Test",
                                    fontFamily: "Inter, sans-serif",
                                    formatter: function(w) {
                                        const sum = w.globals.seriesTotals.reduce((a, b) => {
                                            return a + b
                                        }, 0)
                                        return `${sum}`
                                    },
                                },
                                value: {
                                    show: true,
                                    fontFamily: "Inter, sans-serif",
                                    offsetY: -20,
                                    formatter: function(value) {
                                        return value
                                    },
                                },
                            },
                            size: "80%",
                        },
                    },
                },
                grid: {
                    padding: {
                        top: -2,
                    },
                },
                labels: chartData.labels,
                dataLabels: {
                    enabled: false,
                },
                legend: {
                    position: "bottom",
                    fontFamily: "Inter, sans-serif",
                },
                yaxis: {
                    labels: {
                        formatter: function(value) {
                            return value
                        },
                    },
                },
                xaxis: {
                    labels: {
                        formatter: function(value) {
                            return value
                        },
                    },
                    axisTicks: {
                        show: false,
                    },
                    axisBorder: {
                        show: false,
                    },
                },
            }
        }

        if (document.getElementById("donut-chart") && typeof ApexCharts !== 'undefined') {
            const chart = new ApexCharts(document.getElementById("donut-chart"), getChartOptions());
            chart.render();

            // Get all the checkboxes by their class name
            const checkboxes = document.querySelectorAll('#devices input[type="checkbox"]');

            // Function to handle the checkbox change event
            function handleCheckboxChange(event, chart) {
                const checkbox = event.target;
                if (checkbox.checked) {
                    switch (checkbox.value) {
                        case 'desktop':
                            chart.updateSeries([15.1, 22.5, 4.4, 8.4]);
                            break;
                        case 'tablet':
                            chart.updateSeries([25.1, 26.5, 1.4, 3.4]);
                            break;
                        case 'mobile':
                            chart.updateSeries([45.1, 27.5, 8.4, 2.4]);
                            break;
                        default:
                            chart.updateSeries([55.1, 28.5, 1.4, 5.4]);
                    }

                } else {
                    chart.updateSeries([35.1, 23.5, 2.4, 5.4]);
                }
            }

            // Attach the event listener to each checkbox
            checkboxes.forEach((checkbox) => {
                checkbox.addEventListener('change', (event) => handleCheckboxChange(event, chart));
            });
        }
    });
</script>