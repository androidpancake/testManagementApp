<div class="space-y-2 h-screen">
    @include('success.success')
    @include('errors.error')
    <livewire:head.head title="{{ $title }}" description="{{ $description }}" user_name="{{ auth()->user()->name }}" />
    <div class="flex justify-end items-center space-x-2">
        <button wire:click="create" class="p-2 bg-bsi-primary text-white rounded-lg hover:bg-teal-700">Tambah User</button>
    </div>
    <livewire:users.userlist />
</div>