<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create(
            // [
            //     'name' => 'Raissa',
            //     'unit' => 'ASP',
            //     'department' => 'IT WHA',
            //     'username' => 'ven.benitaraissa',
            //     'password' => Hash::make('Makassarsejahtera04#@')
            // ],
			[
				'name' => 'Admin',
				'unit' => 'ASP',
				'department' => 'QA',
				'username' => 'ven.alvidhea',
				'password' => Hash::make('test123'),
			]
        );
    }
}
