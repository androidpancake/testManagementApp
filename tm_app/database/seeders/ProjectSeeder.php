<?php

namespace Database\Seeders;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Project::create([
            'name' => 'Project Test 1',
            'jira_code' => 'AABBCC',
            'test_level_id' => 1,
            'user_id' => '7186a23b-a78b-486a-b60b-fa6599cc5d6d'
        ]);
    }
}
